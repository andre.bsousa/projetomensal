import CreateUser from '../components/user/create-user'
import ListGame from '../components/game/list-game'
import CreateGame from '../components/game/create-game'
import Home from '../components/home/home'
import Login from '../components/login/login'



const routes = {
    'ListGame': {
      component: ListGame,
      showMenu: true
    },
    'CreateUser': {
      component: CreateUser,
      showMenu: true
    },
  
    'CreateGame': {
      component: CreateGame,
      showMenu: true
    },
  
    'Login': {
      component: Login,
      showMenu: true
    },

    'Home': {
      component: Home,
      showMenu: true
    }
  }

  export default routes