import { clientHttp } from '../config/config'

const createUser = (data) => clientHttp.post(`/user`, data)
const createGame = (data) => clientHttp.post(`/jogos`, data)
const gameDelete = (data) => clientHttp.delete(`/jogos/${data}`)

const gameList = () => clientHttp.get(`/jogos`)

export {
    createUser,
    gameList,
    createGame,
    gameDelete
}