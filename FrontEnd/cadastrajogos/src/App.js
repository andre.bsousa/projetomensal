import React, { useState } from 'react';
import Header from './components/layout/header/header'
import Footer from './components/layout/footer/footer'
import Nav from './components/layout/nav-bar/nav-bar'
import routes from './routes/routes'
import './App.css';






function App() {

  const [page, setPage] = useState('Login')

  const selectedRoute = routes[page]  

  const {component: Page } = routes[page]

  const isShowMenu = (route) => route.showMenu ? <Nav page={route} changePage={setPage} /> : ""



  return (
    <React.Fragment>
      <div className="App">
        <Header />
        {isShowMenu(selectedRoute)}
      </div>
      <main>
        <Page changePage={setPage} />
      </main>
      <Footer />
    </React.Fragment>
  )
}

export default App;
