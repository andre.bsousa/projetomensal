import axios from 'axios'

const clientHttp = axios.create({
    baseURL: `http://localhost:8080`
})

clientHttp.defaults.headers['Content-Type'] = 'application/json'

export {
    clientHttp
}