import React, { useState } from 'react';
import '../login/login.css'


const Login = (props) => {

  const [form, setForm] = useState({})

  const handleChange = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    })
    return

  }

  const isSubmitValid = () => form.email && form.senha

  const submit = () => {
    if (isSubmitValid()) {
      props.changePage('ListGame')

    }

    return

  }

  return (

    <div id="login">
      <div className="form_login">
        <div>
          <label htmlFor="auth_login" className="auth_login">Login: </label>
          <input type="email" id="email" name="email" onChange={handleChange} placeholder="Insira seu e-mail" value={form.email || ""} />

        </div>
        <div>
          <label htmlFor="auth_senha" className="auth_senha">Senha: </label>
          <input type="password" id="senha" name="senha" onChange={handleChange} placeholder="Insira sua senha" value={form.senha || ""} />
        </div>
        <button disabled={!isSubmitValid()} onClick={submit} className="login_button">Logar</button>

      </div>


    </div>

  )
}

export default Login