import React, { useState } from 'react'
import { createUser } from '../../service/route'
import '../user/create-user.css'

const CreateUser = () => {

    const [form, setForm] = useState({})


    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value

        })
        return
    }

    const isSubmitValid = () => form.nome && form.email && form.senha


    const submitForm = async (event) => {

        try {
            await createUser(form)
            alert('Formulario cadastrado com sucesso')
            setForm({})

        } catch (err) {
            console.log('nem foi')

        }


    }




    return (
        <div id="create_user">
            <div className="form_create">
                <div>
                    <label htmlFor="save_nome" className="save_nome">Nome: </label>
                    <input type="text" id="save_nome" name="nome" onChange={handleChange} value={form.nome || ""} placeholder="Insira seu nome"></input>
                </div>
                <div>
                    <label htmlFor="save_email" className="save_email">Email: </label>
                    <input tyoe="email" id="save_email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Insira seu email"></input>
                </div>
                <div>
                    <label htmlFor="save_senha" className="save_senha">Senha: </label>
                    <input type="password" id="save_senha" name="senha" onChange={handleChange} value={form.senha || ""} placeholder="Insira sua senha"></input>
                </div>
                <button disabled={!isSubmitValid()} onClick={submitForm} className="create_button">Cadastrar</button>

            </div>
        </div>

    )

}

export default CreateUser