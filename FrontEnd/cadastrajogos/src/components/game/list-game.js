import React, { useState, useEffect } from 'react'
import { gameList, gameDelete } from '../../service/route'
import Loader from '../loader/loader'
import './game.css'

const ListGame = () => {

    const [games, setGames] = useState([])

    const getList = async () => {
        try {
            const games = await gameList()
            setGames(games.data)

        } catch (err) {
            console.log('deu ruim', err)

        }

    }

    const deleteGame = async ({ _id, nome }) => {

        try {

            if (window.confirm(`Você deseja excluir o jogo ${nome}?`)) {
                await gameDelete(_id)
                getList()
            }


        } catch (err) {
            console.log("Nem foi", err)
        }

    }

    const isGameEmpty = games.length === 0


    const mountTable = () => {


        const lines = games.map((games, index) => (

            <tr key={index}>
                <td>{games.nome}</td>
                <td>{games.genero}</td>
                <td>{games.distribuidora}</td>
                <td>{games.desenvolvedora}</td>
                <td>
                    <span>Editar</span>|
                <span onClick={() => deleteGame(games)}>Excluir</span>
                </td>
            </tr>

        ))


        return !isGameEmpty ? (
            <table>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Genero</th>
                        <th>Distribuidora</th>
                        <th>Desenvolvedora</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {lines}
                </tbody>

            </table>

        ) : ""


    }


    useEffect(() => {
        getList()
    }, [])



    return (
        <div className="list-game">

            <Loader show={isGameEmpty} />
            {mountTable()}
        </div>
    )
}

export default ListGame
