import React, { useState } from 'react';
import { createGame } from '../../service/route'
import Loader from '../loader/loader'
import '../game/game.css'


const CreateGame = (props) => {

    const [form, setForm] = useState({})
    const [isSubmit, setIsSubmit] = useState(false)

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
        return

    }

    const isSubmitValid = () => form.nome && form.genero && form.distribuidora && form.desenvolvedora

    const submitForm = async (event) => {

        try {
            setIsSubmit(true)
            await createGame(form)
            alert('Jogo cadastrado com sucesso')
            setForm({})
            setIsSubmit(false)
            props.changePage('ListGame')

        } catch (err) {
            console.log('deu erro e nem foi')

        }



    }

    return (      

        <div id="create_game">
            <div className="form_create">
                <div>
                    <label htmlFor="save_nome" className="save_nome">Nome</label>
                    <input type="text" id="save_nome" disabled={isSubmit} onChange={handleChange} value={form.nome || ""} name="nome" placeholder="Insira o nome do jogo">                        
                    </input>
                </div>
                <div>
                    <label htmlFor="rpg" className="radio_rpg">RPG</label>
                    <input type="radio" onChange={handleChange} disabled={isSubmit} name="genero" value="RPG"></input>
                    <label htmlFor="fps" className="radio_fps">FPS</label>
                    <input type="radio" name="genero" onChange={handleChange} disabled={isSubmit} value="FPS"></input>
                    <label htmlFor="tps" className="radio_tps">TPS</label>
                    <input type="radio" name="genero" onChange={handleChange} disabled={isSubmit} value="TPS"></input>
                </div>
                <div>
                    <label htmlFor="save_distribuidora" className="save_distribuidora">Distribuidora</label>
                    <input type="text" id="save_distribuidora" disabled={isSubmit} onChange={handleChange} value={form.distribuidora || ""}
                        name="distribuidora" placeholder="Insira o nome da distribuidora"></input>
                </div>
                <div>
                    <label htmlFor="save_desenvolvedora" className="save_desenvolvedora">Desenvolvedora</label>
                    <input type="text" id="save_desenvolvedora" disabled={isSubmit} onChange={handleChange} value={form.desenvolvedora || ""}
                        name="desenvolvedora" placeholder="Insira o nome da desenvolvedora">                            
                        </input>
                </div>
                <button disabled={!isSubmitValid()} onClick={submitForm} className="create_button">Cadastrar</button>

            </div>
            <Loader show={isSubmit} />
        </div>


    )
}

export default CreateGame;