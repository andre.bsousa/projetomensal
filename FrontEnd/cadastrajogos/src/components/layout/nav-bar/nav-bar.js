import React from 'react'
import './nav-bar.css'

const Nav = (props) => {       
    return (    
    <ul>    
        <li onClick={() => props.changePage('Home')}>Home</li>
        <li onClick={() => props.changePage('CreateGame')}>Cadastrar Jogo</li>
        <li>
        <span onClick={() => props.changePage('Login')}>Logar</span>/
        <span onClick={() => props.changePage('CreateUser')}>Cadastrar</span>
        </li>
    </ul>
)}

export default Nav