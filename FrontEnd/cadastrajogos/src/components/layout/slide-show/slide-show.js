import React from 'react';
import { Fade } from 'react-slideshow-image';
import slide1 from '../../../assets/img/slide1.jpg'
import slide2 from '../../../assets/img/slide2.jpg'
import slide3 from '../../../assets/img/slide3.jpg'
import 'react-slideshow-image/dist/styles.css'
 

//TODO - Ajustar o CSS mais a frente dessa budega, para não atrapalhar a tela
const fadeImages = [
    slide1,
    slide2,
    slide3
];
 
const Slideshow = () => {
  return (
    <div className="slide-container">
      <Fade>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[0]} alt=""/>
          </div>         
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[1]} alt=""/>
          </div>         
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[2]} alt=""/>
          </div>         
        </div>
      </Fade>
    </div>
  )
}

export default Slideshow