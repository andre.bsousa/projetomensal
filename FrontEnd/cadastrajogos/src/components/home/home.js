import React from 'react'
import SlideShow from '../layout/slide-show/slide-show'

const Home = () => {
    
    return (
    
    <div>
        <SlideShow />
    </div>
    
    )

}

export default Home