const express = require('express')
const app = express()
var cors = require('cors')
const connectDB = require('./config/db')
const PORT = 8080
var bodyparser = require('body-parser')

app.use(cors())
app.use(express.json())
app.use(bodyparser.urlencoded({extended:true}))
app.use(bodyparser.json())

connectDB()


app.use('/', require('./routes/ola'))
app.use('/envio', require('./routes/api/envio'))
app.use('/jogos', require('./routes/api/jogos'))
app.use('/user', require('./routes/api/user'))

app.listen(PORT, () => console.log(`Rodando em htpp://localhost:${PORT}`))
