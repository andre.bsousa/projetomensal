const mongoose = require('mongoose')

const jogosSchema = new mongoose.Schema({
    nome: {
        type: String,
        require: true,
        unique: true
    },

    genero: {
        type: String,
        require: true
    },

    distribuidora: {
        type: String,
        require: true    
    },

    desenvolvedora: {
        type: String,
        require: true
    }
}, {autoCreate: true})



/*let lista_jogos = []

class Jogos {
    constructor (nome, genero, distribuidora, desenvolvedora){
        this.id = this.gerar_id()
        this.nome = nome
        this.genero = genero
        this.distribuidora = distribuidora
        this.desenvolvedora = desenvolvedora
    }

    gerar_id(){
        if (lista_jogos.length === 0){
            return 1
        }
        return lista_jogos[lista_jogos.length -1].id +1
    }
}    

lista_jogos.push(new Jogos("Elder Scrolls V", "RPG", "Bethesda Softworks", "Bethesda Softworks"))
lista_jogos.push(new Jogos("Fallout 4", "RPG", "Bethesda Softworks", "Bethesda Sofworks"))
lista_jogos.push(new Jogos("Fallout 76", "RPG", "Bethesda Softworks", "Bethesda Sofworks"))*/


module.exports = mongoose.model('Jogos', jogosSchema)